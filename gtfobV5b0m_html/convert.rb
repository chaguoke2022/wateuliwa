#
#    Convert.rb produces a csv file of election candidates from an html file
#    obtained using pdftohtml on gtfobV5b0m.pdf available from www.iebc.or.ke
#
#    Copyright (C) 2022  Benson Muite
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This program is available under GPLv3 or any later version
#
# First convert to HTML from PDF using
# pdftohtml -c gtfobV5b0m.pdf
#
# References on converting an HTML table to CSV format
#
# from https://stackoverflow.com/questions/1403087/how-can-i-convert-an-html-table-to-csv
#
# https://stackoverflow.com/questions/1761306/nokogiri-multiple-css-classes
#

require "nokogiri"

def create_csv(st, en, fields, position)
  Dir.mkdir(position) unless File.exist?(position)
  for i in st..en
    html_file_path = "gtfobV5b0m-" + i.to_s + ".html"
    csv_file_path = position + "/gtfobV5b0m-" + i.to_s + ".csv"
    csv_file = File.new(csv_file_path,"w")
    html_string = File.read( html_file_path )
    doc = Nokogiri::HTML( html_string )
    count=0
    row_num = 0
    doc.xpath( "//div//p[@class='ft04' or @class='ft05' or 
                         @class='ft06' or @class='ft07' or
                         @class='ft013' or @class='ft019' or
                         @class='ft016' or @class='ft010' or
                         @class='ft08' or @class='ft09']" ).each do |row|
                           if row.text.count("a-zA-Z0-9").zero?
                             csv_file.print "\n"
                             row_num = row_num + 1
                             if count != fields
                               puts "File " + i.to_s + " for position " +
                               position + " has " + count.to_s + 
                               " fields instead of " + fields.to_s +
                               " on row " + row_num.to_s
                             end
                             count = 0
                           else 
                             if count > 0
                               csv_file.print ","
                             end
                             csv_file.print row.text
                             count = count + 1
                           end
                         end
    csv_file.close
  end
end

# Position,  Starting page, Ending page, Number of text fields
create_csv(1,2,7,"President")
create_csv(2,166,9,"National_Assembly")
create_csv(166,180,7,"Senate")
create_csv(180,195,7,"County_Woman_Representative")
create_csv(195,208,9,"Governor")

## Manual fixes that occur often

## National Assembly

#NARC-,KENYA -> NARC-KENYA
#027  Jubilee Party -> 027 ,Jubilee Party
#090  Umoja Summit Party -> 090 ,Umoja Summit Party
#065  Kenya Reform Party -> 065 ,Kenya Reform Party
#087  Chama Cha Kazi -> 087 ,Chama Cha Kazi
#034  Safina -> 034 ,Safina
#082  The Service Party -> 082 ,The Service Party
#Tharaka - Nithi  060 -> Tharaka - Nithi  ,060
#036  Chama Cha Uzalendo -> 036 ,Chama Cha Uzalendo
#074  Ukweli Party -> 074 ,Ukweli Party
#040  The New Democrats -> 040 ,The New Democrats
#036  Chama Cha Uzalendo -> 036 ,Chama Cha Uzalendo
#066  Peoples Trust Party -> 066 ,Peoples Trust Party
#050  Muungano Party -> 050 ,Muungano Party
#MUUNG,ANO -> MUUNGANO
# Ind -> ,Ind
#047  Farmers Party -> 047 ,Farmers Party
#068  Democratic Congress -> 068 ,Democratic Congress
#089  Kenya Union Party -> 089 ,Kenya Union Party
#093  Entrust Pioneer Party -> 093 ,Entrust Pioneer Party
#066  Peoples Trust Party -> 066 ,Peoples Trust Party
#Nakuru Town West -> Nakuru Town West ,
#071  National Liberal Party -> 071 ,National Liberal Party
#Bomachoge Borabu -> Bomachoge Borabu ,
#Bomachoge Chache -> Bomachoge Chache ,
#Kitutu Chache North -> Kitutu Chache North ,
#090  Umoja Summit Party -> 090 ,Umoja Summit Party
#Kitutu Chache South -> Kitutu Chache South ,

##SENATE
#National Rainbow Coalition-Kenya  -> National Rainbow Coalition-Kenya ,NARC-KENYA
#Elgeyo Marakwet  -> Elgeyo Marakwet  ,
#Forum For Restoration Of Democracy-Kenya -> Forum For Restoration Of Democracy-Kenya ,FORD-KENYA
#County Code ,Party Code , ->
#Three initial names disappear

##County Woman Representative
#Pamoja African Alliance PAA -> Pamoja African Alliance ,PAA
#Usawa Kwa Wote Party  USAWA  -> Usawa Kwa Wote Party ,USAWA 
#United Green Movement UGM  -> United Green Movement ,UGM 
#Elgeyo Marakwet  -> Elgeyo Marakwet  ,
#Ubuntu Peoples Forum  UPF -> Ubuntu Peoples Forum ,UPF
#United Democratic Party UDP -> United Democratic Party ,UDP
#Federal Party Of Kenya FPK -> Federal Party Of Kenya ,FPK
#Kenya Social Congress KSC -> Kenya Social Congress ,KSC

##Governor
#Taita Taveta -> Taita Taveta ,
#FORD-,KENYA -> FORD-KENYA
#NARC-,KENYA -> NARC-KENYA
#County Code ,County Name ,Political Party Name , ->
#Nyandarua  -> Nyandarua ,
#KADU-,ASILI -> KADU-ASILI
#West Pokot  -> West Pokot ,
#Trans Nzoia  -> Trans Nzoia ,
#Uasin Gishu  -> Uasin Gishu ,
#Homa Bay  -> Homa Bay ,
#Nairobi City  -> Nairobi City ,

