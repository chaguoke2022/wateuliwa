#
#    Convert.rb produces a csv file of election candidates from an html file
#    obtained using pdftohtml on ysKVaE4kBI.pdf available from www.iebc.or.ke
#
#    Copyright (C) 2022  Benson Muite
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# First convert to HTML from PDF using
# pdftohtml -c gtfobV5b0m.pdf
#
# References on convert HTML table to CSV format and using regular expressions
#
# from https://stackoverflow.com/questions/1403087/how-can-i-convert-an-html-table-to-csv
#
# https://stackoverflow.com/questions/1761306/nokogiri-multiple-css-classes
#
# https://stackoverflow.com/questions/7183299/removing-p-elements-with-no-text-with-nokogiri
#
# https://www.rubyguides.com/2019/07/ruby-gsub-method
#

require "nokogiri"

def create_csv(st, en, fields, position)
  Dir.mkdir(position) unless File.exist?(position)
  for i in st..en
    html_file_path = "ysKVaE4kBI-" + i.to_s + ".html"
    csv_file_path = position + "/ysKVaE4kBI-" + i.to_s + ".csv"
    csv_file = File.new(csv_file_path,"w")
    html_string = File.read( html_file_path )
    doc = Nokogiri::HTML( html_string )
    count=0
    doc.xpath( "//div//p[@class='ft02' or @class='ft03']" ).each do |row|
                           row.remove if row.content.strip.empty?
                           if count==(fields-1)
                             csv_file.print ","
                             csv_file.print row.text
                             csv_file.print "\n"
                             count = 0
                           else 
                             if count > 0
                               csv_file.print ","
                             end 
                             # The pattern "NAME XXX" where XXX are digits occurs
                             # occasionally.  These should be separate fields
                             text = row.text.to_s
                             if text.match?(/\D+\d{3}$/)
                               count = count +1
                               text.gsub!(/(\D+)(\d{3})/,'\1'+","+'\2')
                             end
                             # The pattern "XXX IND" occurs occasionally. These
                             # should be separate fields
                             if text.match?(/(\D+)(IND$)/)
                               count = count + 1
                               text.gsub!(/(\D+)(IND$)/,'\1'+","+'\2')
                             end
                             csv_file.print text
                             count = count + 1
                           end
                         end
    csv_file.close
  end
end

# Position,  Starting page, Ending page, Number of text fields
create_csv(1,384,11,"MCA")
