# Wateuliwa

Data ya wateuliwa kutoka https://www.iebc.or.ke

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Candidate lists from https://www.iebc.or.ke

## Kusaidiwa/Support

Swali kwa [GitLab issue](https://gitlab.com/chaguoke2022/wateuliwa/-/issues),
au tuma barua pepe habari@chaguo.info.ke

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Use [GitLab issues](https://gitlab.com/chaguoke2022/wateuliwa/-/issues),
or send an email to habari@chaguo.info.ke

## Ramani ya Jengo/Plan

Tengeneza tovuti watu wanweza kupata wateuliwa 
watakuwa kwa kura ya siri yao.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Make a website where it is easy to query candidates 
that will appear on the ballot in a particular ward.

## Saida/Contribute

Tunapenda saidizi.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Contributions are welcome.

## Shukrani/Acknowledgments

Ongeza jina lako hapa kama umesaidiana.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Add your name if you have made a contribution.


## Lisensi/License

Data ni huru.
Programmu ni GPLv3 au baadaye

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Data is expected to be public domain.
Software is GPLv3 or later.


## Hali/Status

Tunanza.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Initial development.
