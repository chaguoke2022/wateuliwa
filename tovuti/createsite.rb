#
#    createsite.rb produces directories and webpages to find who
#    will be on the ballot in a particular region for the 2022
#    Kenyan General elections
#
#    Copyright (C) 2022  Benson Muite
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This program is available under GPLv3 or any later version

require 'csv'

$top = <<-HTML
  <html lang="sw">
  <!--
    Copyright (C) 2022  Benson Muite

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    This program is available under GPLv3 or any later version
  -->
  <body>
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nani ataonekana kwa kura yako?</title> 
  </head>
  <h1>Nani Ataonekana kwa Kura Yako?</h1>
  Inigiza kaunti, jimbo la uchaguzi na kata yako na utaona.
HTML

$bottom = <<-HTML
 <footer>
  Tuma swali na maoni kwa <email>habari@chaguo.info.ke</email>.<br>
  Wafanyi kazi wa <a href="https://kichakatokizito.solutions">Kichakato Kizito</a>,
  na wengine wametengeneza tovuti hii.<br>
  Tovuti hii ni <a href="https://gitlab.com/chaguoke">huru na wazi.<br>
  </footer>
  </body>
  </html>
HTML

# Read in data
def create_array(path,count)
  d = Dir.open(path)
  csv_files = d.entries.select {|e| /^.+\.csv$/.match(e)}
  d.close
  array = []
  csv_files.select do |e|
    csv_path = path + e
    temp = CSV.read(csv_path)
    temp.each do |row|
      # check for correct number of entries
      if row.length != count
        p row
      end
      array.append(row)
    end
  end
  return array
end

## Wards
$ward_array = create_array('../ysKVaE4kBI_html/MCA/',11)
## Governors
$governor_array = create_array('../gtfobV5b0m_html/Governor/',9)
## Senators
$senate_array = create_array('../gtfobV5b0m_html/Senate/',7)
## County Woman Representatives in the National Assembly
$county_woman_array = create_array('../gtfobV5b0m_html/County_Woman_Representative/',7)
## National Assembly
$national_assembly_array = create_array('../gtfobV5b0m_html/National_Assembly/',9)

# Capitalize words
# https://codereview.stackexchange.com/questions/56961/capitalize-all-words-in-sentence-string-in-ruby
def capitalize_words(string)
  st = string.gsub(" "," ")
  st.gsub(/\S+/, &:capitalize).gsub(" Of "," of ").gsub(" For "," for ").gsub("- Kenya","-Kenya").gsub("-kenya","-Kenya")
end

def createwardpages(county,constituency,ward,national_assembly_table,
                    senate_table,governor_table,county_woman_table)
  # Folder name formatting
  path = "counties/" + county.delete("'  ") + "/" + 
  constituency.delete("'  ") + "/" + capitalize_words(ward.delete("'  "))
  Dir.mkdir(path) unless File.exist?(path)
  ward_file_path = path + "/index.html"

  mca_table = ""
  $ward_array.each do |row|
    current_ward = row[7]
    constituency_name = row[5]
    surname = row[0]
    other_names = row[1]
    party = row[9]
    if constituency.delete("'  ").downcase == constituency_name.delete("'  ").downcase
      if current_ward.delete(" ") == ward.delete(" ")
        if party == "INDEPENDENT"
          party = "ANAYEJITEGEMEA"
        end
        mca_table = mca_table +
          "<tr><td> " + capitalize_words(surname) + 
          "</td><td> " + capitalize_words(other_names) + 
          " </td><td> " + capitalize_words(party) + 
          " </td></tr>"
      end
    end
  end

  ward_file = File.new(ward_file_path,"w")
  middle = <<-HTML
  <h3>Kaunti Yako</h3>
  <div>
  #{county}
  </div>
  <h3>Jimbo la Uchaguzi Wako</h3>
  #{constituency}
  <h3>Kata Lako</h3>
  #{capitalize_words(ward)}
  <h2>Chaguo Chako cha Mwanachama wa Baraza la Kaunti</h2>
  <table id="mca">
  <tr><th> Jina la Mwisho </th><th> Majina Mengine </th><th> Chama </th></tr>
  #{mca_table}
  </table>
  <h2>Chaguo Chako cha Mwanachama wa Baraza la Taifa</h2>
  <table id="national_assembly">
  <tr><th> Jina la Mwisho </th><th> Majina Mengine </th><th> Chama </th></tr>
  #{national_assembly_table}
  </table>
  <h2>Chaguo Chako cha Bibi kutoka Kaunti na Mwanachama wa Baraza la Taifa</h2>
  <table id="woman_county_representative">
  <tr><th> Jina la Mwisho </th><th> Majina Mengine </th><th> Chama </th></tr>
  #{county_woman_table}
  </table>
  <h2>Chaguo Chako cha Seneta</h2>
  <table id="senator">
  <tr><th> Jina la Mwisho </th><th> Majina Mengine </th><th> Chama </th></tr>
  #{senate_table}
  </table>
  <h2>Chaguo Chako cha Gavena na Naibu</h2>
  <table id="governor">
    <tr><th colspan="2"> Gavena </th><th colspan="2"> Naibu wa Gavena </th><th></th></tr>
  <tr><th> Jina la Mwisho </th><th> Majina Mengine </th>
      <th> Jina la Mwisho </th><th> Majina Mengine </th><th> Chama </th></tr>
  #{governor_table}
  </table>
  <h2>Chaguo Chako cha Rais na Naibu</h2>
  <table id="president">
  <tr><th colspan="2"> Rais </th><th colspan="2"> Naibu wa Rais </th><th></th></tr>
  <tr><th> Jina la Mwisho </th><th> Majina Mengine </th>
      <th> Jina la Mwisho </th><th> Majina Mengine </th><th> Chama </th></tr>
  <tr><td> Odinga </td><td>Raila</td><td>Karua</td><td>Martha Wangari</td><td>Azimio la Umoja One Kenya Coalition Party</td></tr>
  <tr><td> Ruto </td><td>William Samoei</td><td>Gachagua</td><td>Rigathi</td><td>United Democratic Alliance</td></tr>
  <tr><td> Waihiga </td><td>David Mwaure</td><td>Mucheru</td><td>Ruth Wambui</td><td>Agano Party</td></tr>
  <tr><td> Wajackoyah </td><td>George Luchiri</td><td>Wamae</td><td>Justina Wangui</td><td>Roots Party of Kenya</td></tr>

  </table>
  <br>
  <a href="../index.html">Rudi</a>
  HTML
  page = $top + middle + $bottom
  ward_file.write page
  ward_file.close
end


def createconstituencypages(county,constituency,senate_table,
                            governor_table,county_woman_table)
  # Folder name formatting
  path = "counties/"+county.delete("'  ") + "/" + constituency.delete("'  ")
  Dir.mkdir(path) unless File.exist?(path)
  constituency_file_path = path + "/index.html"

  national_assembly_table = ""
  $national_assembly_array.each do |row|
    constituency_name = row[3]
    surname = row[4]
    other_names = row[5]
    party = row[7]
    if constituency.delete("'  ").downcase == constituency_name.delete("'  ").downcase
      if party.delete("  ") == "Independent"
         party = "Anayejitegemea"
      end
      national_assembly_table = national_assembly_table +
          "<tr><td> " + capitalize_words(surname) +
          "</td><td> " + capitalize_words(other_names) +
          " </td><td> " + capitalize_words(party) +
          " </td></tr>"
    end
  end

  wards = []
  ward_table = ""
  $ward_array.each do |row|
    ward = row[7]
    constituency_name = row[5]
    if constituency.delete("'  ").downcase == constituency_name.delete("'  ").downcase
      unless wards.include?(ward.delete(" "))
        wards = Array(wards).push(ward.delete(" "))
        ward_table = ward_table + '<option ward="' + capitalize_words(ward.delete("'  ")) + 
          '" value="../../../counties/' + county.delete("'  ") + "/" + 
          constituency.delete("'  ") + "/" + capitalize_words(ward.delete("'  ")) + 
          '/index.html">' + capitalize_words(ward) + '</option>\n'
          createwardpages(county,constituency,ward,national_assembly_table,
                          senate_table,governor_table,county_woman_table)
      end
    end
  end
  constituency_file = File.new(constituency_file_path,"w")
  middle = <<-HTML
  <h3>Kaunti Yako</h3>
  <div>
  #{county.delete("'  ")}
  </div>
  <h3>Jimbo la Uchaguzi Wako</h3>
  #{constituency.delete("'  ")}
  <h3>Kata Lako</h3>
  <select class="ward" id="wardfromoptions" name="wardfromoptions"
     onchange="window.location.href=this.value">
     <option ward="--" value="../../../counties/#{county.delete("'  ")}/#{constituency.delete("'  ")}/index.html">--</option>
  #{ward_table}
  </select><br>
  <a href="../index.html">Rudi</a>
  HTML
  page = $top + middle + $bottom
  constituency_file.write page
  constituency_file.close
end

county_names = ["Mombasa","Kwale","Kilifi","Tana River","Lamu","Taita Taveta",
  "Garissa","Wajir","Mandera","Marsabit","Isiolo","Meru","Tharaka-Nithi",
  "Embu","Kitui","Machakos","Makueni","Nyandarua","Nyeri","Kirinyaga",
  "Murang'a","Kiambu","Turkana","West Pokot","Samburu","Trans-Nzoia",
  "Uasin Gishu","Elgeyo Marakwet","Nandi","Baringo","Laikipia","Nakuru",
  "Narok","Kajiado","Kericho","Bomet","Kakamega","Vihiga","Bungoma","Busia",
  "Siaya","Kisumu","Homa Bay","Migori","Kisii","Nyamira","Nairobi City"]

Dir.mkdir("counties") unless File.exist?("counties")
county_names.each do |county|
  # Folder name formatting
  path = "counties/"+county.delete("' ")
  Dir.mkdir(path) unless File.exist?(path)
  county_file_path = path + "/index.html"
  # Get list of constituencies in the county
  ## https://stackoverflow.com/questions/23505468/in-ruby-how-would-i-find-all-csv-files-in-a-folder-and-print-out-path-to-the-f
  ## https://albertogrespan.com/blog/csv-file-reading-in-ruby/

  senate_table = ""
  $senate_array.each do |row|
    county_name = row[1]
    surname = row[2]
    other_names = row[3]
    party = row[5]
    if county.delete("'  ") == county_name.delete("'  ")
      if party.delete("  ") == "Independent"
         party = "Anayejitegemea"
      end
      senate_table = senate_table +
          "<tr><td> " + capitalize_words(surname) +
          " </td><td> " + capitalize_words(other_names) +
          " </td><td> " + capitalize_words(party) +
          " </td></tr>"
    end
  end

  county_woman_table = ""
  $county_woman_array.each do |row|
    county_name = row[1]
    surname = row[2]
    other_names = row[3]
    party = row[5]
    if county.delete("'  ") == county_name.delete("'  ")
      if party.delete("  ") == "Independent"
         party = "Anayejitegemea"
      end
      county_woman_table = county_woman_table +
          "<tr><td> " + capitalize_words(surname) +
          " </td><td> " + capitalize_words(other_names) +
          " </td><td> " + capitalize_words(party) +
          " </td></tr>"
    end
  end

  governor_table = ""
  $governor_array.each do |row|
    county_name = row[1]
    surname = row[2]
    other_names = row[3]
    vice_surname = row[4]
    vice_other_names = row[5]
    party = row[7]
    if county.delete("'  ") == county_name.delete("'  ")
      if party.delete("  ") == "Independent"
         party = "Anayejitegemea"
      end
      governor_table = governor_table +
          "<tr><td> " + capitalize_words(surname) +
          " </td><td> " + capitalize_words(other_names) +
          " </td><td> " + capitalize_words(vice_surname) +
          " </td><td> " + capitalize_words(vice_other_names) +
          " </td><td> " + capitalize_words(party) +
          " </td></tr>"
    end
  end

  constituencies = []
  constituency_table = ""
  $national_assembly_array.each do |row|
    constituency = row[3]
    county_name = row[1]
    if county.delete("'  ") == county_name.delete("'  ")
      unless constituencies.include?(constituency.delete(" "))
        constituencies = Array(constituencies).push(constituency.delete(" "))
        constituency_table = constituency_table +
          '<option constituency="' + constituency.delete("'  ") + 
          '" value="../../counties/' + county.delete("'  ") + "/" + 
          constituency.delete("'  ") + '/index.html">' + 
          constituency + '</option>\n'
          createconstituencypages(county_name,constituency,senate_table,
                                  governor_table,county_woman_table)
      end
    end
  end
  county_file = File.new(county_file_path,"w")
  middle = <<-HTML
  <h3>Kaunti Yako</h3>
  <div>
  #{county}
  </div>
  <h3>Jimbo la Uchaguzi Wako</h3>
  <select class="constituency" id="constituencyfromoptions" name="constituencyfromoptions"
     onchange="window.location.href=this.value">
     <option constituency="--" value="../../counties/#{county}/index.html">--</option>
  #{constituency_table}
  </select><br>
  <a href="../../index.html">Rudi</a>
  HTML
  page = $top + middle + $bottom
  county_file.write page
  county_file.close
end
